const got = require('got');
const semver = require('semver');
const FormData = require('form-data');

const {
  DOCKER_REPOSITORY = 'm03geek/alpine-node',
  LAST_SUPPORTED_VERSION = '8.0.0',
  IMAGE_TAG_PREFIX = 'base',
  IMAGE_TAG_VERSION_PREFIX = '-',
  TRIGGER_URL = 'https://gitlab.com/api/v4/projects/13636902/trigger/pipeline',
  TRIGGER_TOKEN,
} = process.env;

const NODE_VERSIONS_URL = 'https://nodejs.org/dist/index.json';
const DOCKERHUB_API = `https://hub.docker.com/v2/repositories/${DOCKER_REPOSITORY}/tags`;

async function getDockerVersionsPage(page) {
  const data = await got(DOCKERHUB_API, {json: true, query: {page, page_size: 50}});
  return data.body;
}

async function getDockerVersions() {
  const data = [];
  let page = 1;
  let next = true;

  while (next) {
    const res = await getDockerVersionsPage(page);
    next = res.next;
    data.push(...res.results);
    page++;
  }

  return data;
}

async function getNodeVersions() {
  const data = await got(NODE_VERSIONS_URL, {json: true});
  return data.body;
}

function cleanSemver(it) {
  return semver.clean(it, {loose: true});
}

function filterSupported(it) {
  return semver.gt(it, LAST_SUPPORTED_VERSION);
}

function reduceLatestMajor(acc, it) {
  const major = semver.major(it);
  if (
    !acc.find((latest) => {
      const m = semver.major(latest);
      return m === major;
    })
  ) {
    acc.push(it);
  }
  return acc;
}

(async () => {
  const [nodeVersions, dockerVersions] = await Promise.all([
    getNodeVersions(),
    getDockerVersions(),
  ]);

  const nv = nodeVersions
    .map((it) => {
      return it.version;
    })
    .map(cleanSemver)
    .filter(filterSupported)
    .sort(semver.rcompare)
    .reduce(reduceLatestMajor, []);
  const dv = dockerVersions
    .filter((it) => {
      return it.name.includes(IMAGE_TAG_PREFIX);
    })
    .map((it) => {
      return it.name.split(IMAGE_TAG_VERSION_PREFIX)[1];
    })
    .filter(filterSupported)
    .map(cleanSemver)
    .sort(semver.rcompare)
    .reduce(reduceLatestMajor, []);
  const diff = nv.filter((it) => {
    return !dv.includes(it);
  });
  if (!diff.length) {
    return;
  }

  await Promise.all(
    diff.map((it) => {
      const form = new FormData();
      form.append('ref', 'master');
      form.append('token', TRIGGER_TOKEN);
      form.append('variables[BUILD]', 'true');
      form.append('variables[VERSION]', it);
      return got(TRIGGER_URL, {
        method: 'POST',
        body: form,
      });
    })
  );
})();
