#!/bin/bash

version=$1
echo "Building version $version"

git tag -d "v$version" || true
git push --delete --force origin "v$version" || true
git tag "v$version"
git push
git push --tags
