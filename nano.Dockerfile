ARG version=0.0.0

FROM m03geek/alpine-node:micro-${version} as build
RUN apk add --no-cache binutils-gold binutils upx
RUN strip /usr/bin/node

FROM alpine:latest
COPY --from=build /usr/bin/node /usr/bin
COPY docker-entrypoint.sh /usr/bin
RUN addgroup -g 1000 node \
  && adduser -u 1000 -G node -s /bin/sh -D node \
  && chmod a+x /usr/bin/docker-entrypoint.sh \
  && ln -s /usr/bin/node /usr/local/bin/node
ENTRYPOINT [ "/usr/bin/docker-entrypoint.sh" ]
CMD ["node"]
