ARG version=0.0.0

FROM m03geek/alpine-node:micro-${version} as build
RUN apk add --no-cache binutils-gold binutils upx
RUN strip /usr/bin/node
RUN upx /usr/bin/node 
RUN echo 'node:x:1000:1000:Linux User,,,:/home/node:/bin/sh' > /tmp/passwd

FROM scratch
COPY --from=build /usr/bin/node /bin/node
COPY --from=build /tmp/passwd /etc/passwd
USER node
CMD [ "node" ]