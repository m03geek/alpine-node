# Node static docker builds

## Table of contents

- [Node static docker builds](#node-static-docker-builds)
  - [Table of contents](#table-of-contents)
  - [Info](#info)
  - [Usage](#usage)
    - [Pulling](#pulling)
    - [Available tags](#available-tags)
  - [Versions](#versions)
    - [Milli](#milli)
      - [Docker](#docker)
      - [Features](#features)
    - [Micro](#micro)
      - [Docker](#docker-1)
      - [Features](#features-1)
    - [Nano](#nano)
      - [Docker](#docker-2)
      - [Features](#features-2)
    - [Pico](#pico)
      - [Docker](#docker-3)
      - [Features](#features-3)
    - [Femto](#femto)
      - [Docker](#docker-4)
      - [Features](#features-4)
  - [Images with yarn, npm](#images-with-yarn-npm)

## Info

The main purpose of this images is to provide small footprint containers that can be used in multistage builds, when npm and yarn not needed.

Builds are fully automated. Script checks availability of new node versions each 6 hours.

## Usage

### Pulling

```
docker pull m03geek/alpine-node:$TAG
```

### Available tags

- `m03geek/alpine-node:$TYPE`, example: `m03geek/alpine-node:nano` (latest)
- `m03geek/alpine-node:$TYPE-$MAJOR`, example: `m03geek/alpine-node:nano-12`
- `m03geek/alpine-node:$TYPE-$MAJOR.$MINOR`, example: `m03geek/alpine-node:nano-12.8`
- `m03geek/alpine-node:$TYPE-$MAJOR.$MINOR.$PATCH`, example: `m03geek/alpine-node:nano-12.8.0`

## Versions

Node version builds starting from:

- `>=8.16.0`
- `>=9.11.2`
- `>=10.16.2`
- `>=11.15.0`
- `>=12.8.0`

### Milli

#### Docker

Dockerfile: [milli.Dockerfile](https://gitlab.com/m03geek/alpine-node/blob/master/milli.Dockerfile)

#### Features

- Static node.js build.
- No yarn.
- No npm.

### Micro

#### Docker

Dockerfile: [micro.Dockerfile](https://gitlab.com/m03geek/alpine-node/blob/master/micro.Dockerfile)

#### Features

- Static node.js build.
- No yarn.
- No npm.
- Inspector disabled
- Dtrace disabled

### Nano

#### Docker

Dockerfile: [nano.Dockerfile](https://gitlab.com/m03geek/alpine-node/blob/master/nano.Dockerfile)

#### Features

- Static node.js build.
- No yarn.
- No npm.
- Inspector disabled
- Dtrace disabled
- Stripped debug symbols

### Pico

#### Docker

Dockerfile: [pico.Dockerfile](https://gitlab.com/m03geek/alpine-node/blob/master/pico.Dockerfile)

#### Features

- Static node.js build.
- No yarn.
- No npm.
- Inspector disabled
- Dtrace disabled
- Stripped debug symbols
- Executable packed with [upx](https://upx.github.io/)

### Femto

#### Docker

NOTE: Based on `scratch`.

Dockerfile: [femto.Dockerfile](https://gitlab.com/m03geek/alpine-node/blob/master/femto.Dockerfile)

#### Features

- Static node.js build.
- No yarn.
- No npm.
- Inspector disabled
- Dtrace disabled
- Stripped debug symbols
- Executable packed with [upx](https://upx.github.io/)
- Image based on [scratch](ttps://hub.docker.com/_/scratch)

## Images with yarn, npm

If you need images with yarn and npm there are sevral options:

- Node official images [Docker](https://hub.docker.com/_/node), [Github](https://github.com/nodejs/docker-node)
- Mhart's alpine images [Docker](https://hub.docker.com/r/mhart/alpine-node), [Github](https://github.com/mhart/alpine-node)
